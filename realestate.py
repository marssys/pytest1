# Комментарий
# ТУДУ: Отправка e-mail lussa18@yandex.ru со ссылкой на документ Google

import gspread
from oauth2client.service_account import ServiceAccountCredentials

def print_current_data(month):
    print('\nТекущие данные (' + worksheet.cell(int(month) + 1, 1).value + '):')
    print('1. Аренда = ' + worksheet.cell(int(month) + 1, 2).value)
    print('2. Квартплата(-1) = ' + worksheet.cell(int(month) + 1, 3).value)
    print('3. Капремонт(-1) = ' + worksheet.cell(int(month) + 1, 4).value)
    print('4. Эл. расчет(-1) = ' + worksheet.cell(int(month) + 1, 5).value)
    print('5. Всего = ' + worksheet.cell(int(month) + 1, 6).value)
    print('6. Дата оплаты = ' + worksheet.cell(int(month) + 1, 7).value)
    print('7. Показания эл. счетчика = ' + worksheet.cell(int(month) + 1, 8).value)

print('1. Долгоозерная (DOL)')
print('2. Зеленина (ZEL)')
id = input('Какой объект? ')

# Общие данные
scope = ['https://spreadsheets.google.com/feeds']
credentials = ServiceAccountCredentials.from_json_keyfile_name('Programming-de716541da8d.json', scope)
gc = gspread.authorize(credentials)

# Открываем документ
sh = gc.open('DOL Людмила')

# Запрашиваем год
year = input('Какой год? ')

# Открываем лист соответствующий году
worksheet = sh.worksheet(year)

# Запрашиваем месяц
imonth = input('Какой месяц (1-12)? ')

# Выводим текущие данные за этот месяц
# Тестовый комментарий
print_current_data(imonth)