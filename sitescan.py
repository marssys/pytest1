# ТУДУ: добавить описание скрипта

import requests
from bs4 import BeautifulSoup

# Базовые параметры Yandex
yaurl = 'https://yandex.ru/search/?text=site:' # +tld

# Базовые параметры Google
gurl = 'https://www.google.ru/search?q=site:' # +tld

# Функция для определения количества результатов в Yandex
def yaq(tld):
    soup = BeautifulSoup(requests.get(yaurl + tld).text, 'lxml')
    if soup.find("div", class_="serp-adv__found") == None:
        res = '0'
    else:
        res = soup.find("div", class_="serp-adv__found").get_text()
    return res

# Функция для определения количества результатов в Google
def gq(tld):
    soup = BeautifulSoup(requests.get(gurl + tld).text, 'lxml')
    if soup.find("div", id="resultStats").get_text() == '':
        res = '0'
    else:
        res = soup.find("div", id="resultStats").get_text()
    return res

# Запрос top-level domain
tld = input('Введите TLD: ')
print('Yandex: ' + yaq(tld))
print('Google: ' + gq(tld))